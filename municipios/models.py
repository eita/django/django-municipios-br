from django.db import models


###############
# Geograficas #
###############

class Diocese(models.Model):
    nome = models.CharField(max_length=500)


class Regiao(models.Model):
    nome = models.CharField(max_length=500)


class UF(models.Model):
    nome = models.CharField(max_length=500)
    sigla = models.CharField(max_length=500)
    regiao = models.ForeignKey(Regiao, on_delete=models.SET_NULL, null=True)


class RegiaoIntermediaria(models.Model):
    nome = models.CharField(max_length=500)
    uf = models.ForeignKey(UF, on_delete=models.SET_NULL, null=True)


class RegiaoImediata(models.Model):
    nome = models.CharField(max_length=500)
    regiao_intermediaria = models.ForeignKey(RegiaoIntermediaria, on_delete=models.SET_NULL, null=True)


class Municipio(models.Model):
    nome = models.CharField(max_length=500)
    regiao_imediata = models.ForeignKey(RegiaoImediata, on_delete=models.SET_NULL, null=True)
    uf = models.ForeignKey(UF, on_delete=models.SET_NULL, null=True)
    diocese = models.ForeignKey(Diocese, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.nome
