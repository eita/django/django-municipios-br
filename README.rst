=============
Municipios-BR
=============

Este é um simples app Django com dados do IBGE de municipio, estado (uf), regiões imediatas e intermediárias, de acordo com o `recorte de 2017 <https://biblioteca.ibge.gov.br/visualizacao/livros/liv100600.pdf>`_.


Instalação
----------
0. Instale o pacote::

    pip3 install -e 'git+https://gitlab.com/eita/django/django-municipios-br.git@master#egg=django-municipios-br'

1. Inclua "municipios" na configuração INSTALLED_APPS::

    INSTALLED_APPS = [
        ...
        'municipios',
    ]

.. 2. Include the municipios URLconf in your project urls.py like this::

..    path('municipios/', include('municipios.urls')),

3. Run `python manage.py migrate` to create the municipios models.

4. Importe os dados do IBGE 2017::

    python manage.py loaddata ibge_2017

5. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a municipio (you'll need the Admin app enabled).

6. Visit http://127.0.0.1:8000/municipios/ to see municipios.


Para desenvolvimento
--------------------

1. Faça um clone deste repositorio em uma pasta separada ao do teu projeto

2. Faça um link simbolico da pasta **municipios** para dentro do teu projeto, como um app

3. Siga a partir do passo 3 da instalação